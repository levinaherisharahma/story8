from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .views import *

# javascript ga usah dites karena merupakan client-side-server
class Test(TestCase):
	def test_ada_url_kosong(self):
		c = Client()
		response = c.get('//')
		self.assertEqual(response.status_code, 200)

	def test_halaman_utama_menggunakan_index_html(self):
		c = Client()
		response = c.get('//')
		self.assertTemplateUsed(response, 'index.html')

	def test_halaman_utama_pakai_fungsi_index(self):
		function = resolve('/')
		self.assertEqual(function.func, index)

	def test_pakai_fungsi_get_data(self):
		function = resolve('/getData/<str:key>')
		self.assertEqual(function.func, get_data)

	def test_include_jquery(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("https://code.jquery.com/jquery-3.4.1.min.js", content)

	def test_ada_script(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<script", content)

	def test_ada_search_box(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<input", content)
		self.assertIn("text", content)

	def test_ada_button_cari(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<input", content)
		self.assertIn("submit", content)

	def test_ada_table_hasil(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<table", content)


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story8FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story8FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story8FunctionalTest, self).tearDown()

	def test_cari_buku(self):
		selenium = self.selenium
		# buka link yg mau dites
		selenium.get(self.live_server_url)

		# cari elemen
		searchbox = selenium.find_element_by_id('box')
		button = selenium.find_element_by_id('button')

		# isi search box
		searchbox.send_keys('Dr. Stone')

		# klik tombol cari buku
		button.send_keys(Keys.RETURN)

		